Install/ Run Instruction
1. Download and Install Visual Studio 2017 from https://visualstudio.microsoft.com/
2. Download/Clone the repositry to your local machine.
3. Open the Population_Calculator Folder.
4. Double Click (or Open with) Population_Calculator.sln file to run in Visual Studio 2017.
5. Press the Green Start Icon on Top-Center in Visual Studio to run the Application
6. Follow the instructions provided on the UI of the Application to provide valid input.
7. The application calculates the total population of an organism provided the starting population,
   daily population increase % and total days allowed for multiplication. Calculate Button calculates
   the total population, Save button saves the results and Clear button clears the input values.

Note: Following work is copyrighted. More Details at: https://creativecommons.org/licenses/by-sa/4.0/legalcode. 

                    Copyright Samaksh Gollen, 2019. 

Creative Commons Attribution-ShareAlike 4.0 International Public License.


Copyright Reasoning: I used the Creative Commons Attribution-ShareAlike license because it prohibits the use 
of the application for commercial purposes/monetary gain. It protects us against plagarism and inappropriate use
of the application. On the other hand, it gives other users freedom to use the application for education/learning purposes
and to make enchancements.



Note: Please feel free to contact me if you require further assistance. 

