﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

// Group 2, Assignment 4, Oct 9, 2018
namespace Population_Calculator
{
    class ViewModel : INotifyPropertyChanged
    {
        #region PropertyDefinition

        // define properties used for interaction with XAML sheet. Includes textboxes for input, indexs of List box, and labels for output
        private BindingList<string> data = new BindingList<string>();
        public BindingList<string> Data
        {
            get { return data; }
            set { data = value; }
        }

        private uint startingPopulation = 0;
        public uint StartingPopulation
        {
            get { return startingPopulation; }
            set { startingPopulation = value; NotifyChanged(); }
        }

        private string startingPopulationInfo = "Please enter organism starting population";
        public string StartingPopulationInfo
        {
            get { return startingPopulationInfo; }
            set { startingPopulationInfo = value; NotifyChanged(); }
        }

        private float percentIncrease = 0;
        public float PercentIncrease
        {
            get { return percentIncrease; }
            set { percentIncrease = value; NotifyChanged(); }
        }

        private uint numberOfDays = 0;
        public uint NumberOfDays
        {
            get { return numberOfDays; }
            set { numberOfDays = value; NotifyChanged(); }
        }

        private bool backwardCount;
        public bool BackwardCount
        {
            get { return backwardCount; }
            set { backwardCount = value; Clear(); NotifyChanged(); }
        }

        private int selectedIndex = -1;
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set { selectedIndex = value; CalculatePopulation(); NotifyChanged(); }
        }

        private string totalPopulationInfo;
        public string TotalPopulationInfo
        {
            get { return totalPopulationInfo; }
            set { totalPopulationInfo = value; NotifyChanged(); }
        }

        private uint totalPopulation;
        public uint TotalPopulation
        {
            get { return totalPopulation; }
            set { totalPopulation = value; NotifyChanged(); }
        }
        #endregion

        #region Logic

        /* A method/function that calculates the total population (given starting population) or initial population
         * (given total population) of an organism. The function also requires a percent increase per day 
         * and number of days to multiply for the calculation.
         */
        public void CalculatePopulation()
        {
            // Constants for later use
            const int HUNDERED_PERCENT = 100;
            const int NUMBER_ONE = 1;

            // Variable(s) for later use
            uint currentPopulation = (uint)StartingPopulation;

            // loop that calculates the population depending on the radio button selected and displays on labels
            for (int Index = 0; Index < NumberOfDays; Index++)
            {
                if (BackwardCount)
                {
                    TotalPopulationInfo = $"The total popultaion of the organism at day {((SelectedIndex < 0) ? 0 : SelectedIndex)} was:";
                    if (SelectedIndex == NumberOfDays - Index)
                    {
                        TotalPopulation = currentPopulation;
                        break;
                    }
                    currentPopulation = (uint)(currentPopulation / (NUMBER_ONE + (PercentIncrease / HUNDERED_PERCENT)));
                }
                else
                {
                    TotalPopulationInfo = $"The total popultaion of the organism after {((SelectedIndex < 0) ? (int)NumberOfDays : SelectedIndex)}  day(s) is:";
                    if (SelectedIndex == Index)
                    {
                        TotalPopulation = currentPopulation;
                        break;
                    }
                    currentPopulation += (uint)(currentPopulation * (PercentIncrease / HUNDERED_PERCENT));
                }
                TotalPopulation = currentPopulation;
            }
        }

        // A method that clears various labels, text boxes and listboxes
        public void Clear()
        {
            StartingPopulationInfo = $"Please enter organism {(BackwardCount ? "final" : "starting")} population";
            Data.Clear();
            TotalPopulation = 0;
            TotalPopulationInfo = "";
            StartingPopulation = 0;
            NumberOfDays = 0;
            PercentIncrease = 0;
            SelectedIndex = -1;
        }

        // A method that saves the calculated population output to a file
        public void Save()
        {
            string fileText = $"The total popultaion of the oraganism after {NumberOfDays} day(s) is {TotalPopulation}";
            fileText += Environment.NewLine;
            string appPath = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            string dataPath = Path.Combine(appPath, "PROG8010");
            string filePath = Path.Combine(dataPath, "output.txt");
            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }
            File.WriteAllText(filePath, fileText);
        }

        // A method that sets up the listbox items for the listbox on the xaml window
        public void SetListBox()
        {
            Data.Clear();
            Data.Add("Initial Population");
            for (int Index = 0; Index < NumberOfDays; Index++)
                Data.Add($"Day {Index + 1}");
        }

        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}