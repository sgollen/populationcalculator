﻿using System;
using System.IO;
using System.Windows;

//Group 2, Assignment 4, Oct 9, 2018
namespace Population_Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //initializing the VM class Object
        ViewModel vm = new ViewModel();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        //event handler for Save button click. Calls the Save() method of the ViewModel class object,
        // enables clear button and disables save button
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.IsEnabled = false;
            BtnClear.IsEnabled = true;
            vm.Save();
        }

        //event handler for Clear button click. Calls the Clear() method of the ViewModel class object,
        // enables the Save button and disables clear button
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.IsEnabled = true;
            BtnClear.IsEnabled = false;
            vm.Clear();
        }

        //event handler for Calculate button click. Calls the CalculatePopulation() and SetListBox() method
        //of the ViewModel class object, enables the Save button and clear button
        private void BtnCalculate_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.IsEnabled = true;
            BtnClear.IsEnabled = true;
            vm.SetListBox();
            vm.CalculatePopulation();
        }
    }
}
